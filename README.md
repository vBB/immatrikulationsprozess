# Digitization of matriculation
### General

Digitalisierung des Immatrikulationsprozesses

- Frontend: Vue.js 
- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut 
- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut 
- Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut 

#### Version information
| Purpose | Tool  | Version |
| ------ | ------ | ------ |
| Frontend | VueJS | 0 |
| Backend (API) | Node | 0 |
| Database | MySQL | 0 |
| Process Automation | Camunda Engine | 0 |

#### Used ports
| Tool | Port |
| ------ | ------ |
| Frontend | 3000 |
| Server | 4000 |
| Camunda | 8080 |

---
### Installation Checklist

- [x] Clone Repository
- [ ] Install MySQL local
- [ ] Install Camunda Engine local
- [ ] Deploy BPMN, DMN and Forms
- [ ] Deploy Database (Setup configuration file)
- [ ] Run Frontend
- [ ] Run Backend

---
### Deploy
#### BPMN, DMN and Form´s
```
bash camunda/upload.bat
```

#### Database
```
Script to generate and fill the database will follow
```
### Run
#### Frontend *(for development)*
```
npm run serve
```

### Backend
```
node server/app.js
```